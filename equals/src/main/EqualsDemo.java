package main;

import lib.Name;

public class EqualsDemo {

	public static void main(String[] args) {
		
		
		System.out.println("exercise 3.5.a-------------"); 
		
		String a = new String("hello");
		String b = new String("hello");
		System.out.println("a==b is: " + (a==b)); //false
		System.out.println("a.equals(b) is: " + a.equals(b)); //true
		
		
		System.out.println("exercise 3.5.b-------------"); 

		Name n = new Name("Joe", "Bloggs");
		Name n1 = new Name("Joe", "Bloggs");
		System.out.println("n==n1 is: " + (n==n1));
		System.out.println("n.equals(n1) is: " + n.equals(n1));
		
		/*
		 * Last assertion is false because the Name class does not 
		 * implement a custom equal method on the Object name
		 */

		System.out.println("exercise 3.5.b-------------"); 
		/*
		 * when overriding equal method is implented in Name class,
		 * the last assertion comes out true because it is adapted to the 
		 * name object compared
		 * 
		 */
}

}
