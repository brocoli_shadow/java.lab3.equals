package lib;

/**
 * A name has a first name and a family name.
 * There is an option to produce a full name by combining these.
 * 
 * @author Luke
 */
public class Name {

	//Fields
	private String firstName;
	private String familyName;

	/**Default constructor initialise first and family name to null string*/
	//Constructors
	public Name() {
		firstName = "";
		familyName = "";
	}
	/**
	 * Initialise a newly created Name object with a custom first name and 
	 * a custom family name
	 * @param firstName The initial value of the first name.
	 * @param familyName The initial value of the family name.
	 */
	public Name(String firstName, String familyName) {
		this.firstName = firstName;
		this.familyName = familyName;
	}

	
	//Methods
	/**
	 * Sets the value of the first name to that specific argument
	 * @param firstName The string value to which the first name should be set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * Sets the value of the family name to that specific argument
	 * @param familyName The string value to which the family name should be set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	/**
	 * Returns the current value of the first name.
	 * @return The current value of the first name
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * Returns the current value of the family name.
	 * @return The current value of the family name
	 */
	public String getFamilyName() {
		return familyName;
	}
	/**
	 * Returns the current value of the first name plus the family name if they
	 * are not null otherwise return blank.
	 * @return The current value of the full name
	 */
	public String getFullName() {
		if (firstName.equals("") && familyName.equals("")) {
			return "";
		} else {
			return firstName + " " + familyName;
		}
	}
	/**
	 * Returns a textual representation of the Name object
	 * @return A textual representation of the Name object.
	 */
	@Override
	public String toString() {
		return "Name:[firstName=" + firstName + ", familyName=" + familyName + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		// test exceptional cases, i.e. obj not null and is a Name
		if (obj == null || this.getClass() != obj.getClass())
			return false;

		Name other = (Name) obj; // downcast to a Name object

		// compare first & family names using String' .equals() method
		return this.familyName.equals(other.familyName)
			&& this.firstName.equals(other.firstName);
	}

	

}
